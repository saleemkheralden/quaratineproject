function darkMode(domainName) {

    document.body.classList.toggle('dark-theme');


    if (document.body.classList["value"] == "dark-theme") {
        document.cookie = "dark=true";
        document.getElementById("twitter-logo").src = domainName + "/static/images/white-twitter-logo.png";
        document.getElementById("instagram-logo").src = domainName + "/static/images/white-instagram-logo.png";
    } else {
        document.cookie = "dark=false";
        document.getElementById("twitter-logo").src = domainName + "/static/images/twitter-logo.png";
        document.getElementById("instagram-logo").src = domainName + "/static/images/instagram-logo.png";
    }

}

function check(domainName) {

    if (document.cookie.includes('dark=true')) {
        document.body.classList.toggle('dark-theme');
        document.getElementById("twitter-logo").src = domainName + "/static/images/white-twitter-logo.png";
        document.getElementById("instagram-logo").src = domainName + "/static/images/white-instagram-logo.png";
        document.getElementById("checkbox").checked = true;
    } else {
        document.getElementById("twitter-logo").src = domainName + "/static/images/twitter-logo.png";
        document.getElementById("instagram-logo").src = domainName + "/static/images/instagram-logo.png";
        document.getElementById("checkbox").checked = false;
    }
}





