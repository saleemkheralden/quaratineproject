var data;

function extract_user_data(user_data) {
    data = user_data;

    var table = document.getElementById('profile_table');

    var table_elem = "<tr><td><label for='name-input' class='profile_label'>Name:</label></td></tr><tr><td><input type='text' class='profile_input' id='name-input' value='" + user_data.name + "' name='name-input'></td></tr>";
    table_elem += "<tr><td><label for='last_name-input' class='profile_label'>Last name:</label></td></tr><tr><td><input type='text' class='profile_input' id='last_name-input' value='" + user_data.last_name + "'name='last_name-input'></td></tr>";
    table_elem += "<tr><td><label for='username-input' class='profile_label'>Username:</label></td></tr><tr><td><input type='text' class='profile_input' id='username-input' value='" + user_data.username + "' name='username-input'></td></tr>";
    table_elem += "<tr><td><label for='email-input' class='profile_label'>Email:</label></td></tr><tr><td><input type='text' class='profile_input' id='email-input' value='" + user_data.email + "' name='email-input'></td></tr>";
    table_elem += "<tr><td><label for='phone_number-input' class='profile_label'>Phone number:</label></td></tr><tr><td><input type='text' minlength='10' maxlength='10' class='profile_input' id='phone_number-input' value='" + user_data.phone_number + "' name='phone_number-input'></td></tr>";

    table.innerHTML = table_elem;
}

function update_profile() {
    delete data.admin;
    delete data.password;
    delete data.id;

    var keys = Object.keys(data);
    var request = "";
    var send = false;
    keys.forEach(function(e) {
        var elem = document.getElementById(e + "-input").value;

        request += e + ':' + elem + '\n';

        if (elem != data[e])
            send = true;
    });

    if (send == true) {
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "profile", true);
        xhttp.send(request);
    }
}