var len = 0;
var _posts;

function addPosts(posts) {
    _posts = posts;
    len = posts.length;
    var post = '';
    var container = document.getElementById('posts_container');
    for (let i = 0; i < posts.length; i++) {
        post = '<div class="outer_div"';

        if (i == 0) {
            post += " style = 'margin-top: 30px;' "
        }

        post += '>';
        post += '<div class="inner_div">';
        post += '<div>';
        post += '<span id="name_span_' + i + '">';
        post += posts[i].name;
        post += '</span>';
        post += '</div>';
        post += '<div>';
        post += '<span id="date_span_' + i + '">';
        post += posts[i].date;
        post += '</span>';
        post += '</div>';
        if (posts[i].description != '') {
            post += '<div id="description_div_' + i + '" class="description_div">';
            post += '<p id="description_par_' + i + '">';
            post += posts[i].description;
            post += '</p>';
            post += '</div>';
        }
        post += '</div>';
        post += '<div class="inner_div">';
        post += '<div>';
        post += '<div id="map_' + i + '" style="height: 500px;">';
        post += '</div>';
        post += '</div>';
        post += '</div>';
        post += '</div>';
        post += '';
        container.innerHTML += post;
    }
    initMap(posts);
}

function initMap(posts) {
    //console.log(posts);

    for (let i = 0; i < len; i++) {
        var haightAshbury = { lat: parseFloat(posts[i].route[0][0]), lng: parseFloat(posts[i].route[0][1])};
        var n = Math.sqrt(Math.pow((parseFloat(posts[i].route[0][0]) - parseFloat(posts[i].route[1][0])), 2) + Math.pow((parseFloat(posts[i].route[0][1]) - parseFloat(posts[i].route[1][1])), 2));
        var zoom = 1/(n*100);
        console.log(zoom);
        map = new google.maps.Map(document.getElementById("map_" + i), {
            zoom: zoom,
            center: haightAshbury,
            mapTypeId: "terrain",
        });

        for (let j = 0; j < posts[i].route.length; j++) {
            addMarker(j, map, { lat: parseFloat(posts[i].route[j][0]), lng: parseFloat(posts[i].route[j][1])});
        }

    }
}

function addMarker(number, map, location) {
      const marker = new google.maps.Marker({
          position: location,
          title: (number+1).toString(),
          label: (number+1).toString(),
          map: map,
      });
}

