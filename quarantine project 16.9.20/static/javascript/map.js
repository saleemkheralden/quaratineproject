// In the following example, markers appear when the user clicks on the map.
// The markers are stored in an array.
// The user can then click an option to hide, show or delete the markers.
let map;
let markers = [];
var latv;
var lngv;

function initMap() {
    var haightAshbury;
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function (position) {
            //console.log("Latitude: " + position.coords.latitude + " , Longitude: " + position.coords.longitude);
            latv = position.coords.latitude;
            lngv = position.coords.longitude;
            haightAshbury = { lat: latv, lng: lngv};
            map = new google.maps.Map(document.getElementById("map"), {
                zoom: 12,
                center: haightAshbury,
                mapTypeId: "terrain",
            });

            map.addListener("click", (event) => {
                addMarker(event.latLng);
            });

            addMarker(haightAshbury);
        });
    }

}

// Adds a marker to the map and push to the array.
function addMarker(location) {
  if (!compareLocation(location)) {
      const marker = new google.maps.Marker({
          position: location,
          title: (markers.length+1).toString(),
          label: (markers.length+1).toString(),
          map: map,
      });
  const contentString =
    '<div id="content" style="color: black">' +
    '<div id="siteNotice">' +
    "</div>" +
    '<h1 id="firstHeading" class="firstHeading">Uluru</h1>' +
    '<div id="bodyContent">' +
    "<p><b>Uluru</b>, also referred to as <b>Ayers Rock</b>, is a large " +
    "sandstone rock formation in the southern part of the " +
    "Northern Territory, central Australia. It lies 335&#160;km (208&#160;mi) " +
    "south west of the nearest large town, Alice Springs; 450&#160;km " +
    "(280&#160;mi) by road. Kata Tjuta and Uluru are the two major " +
    "features of the Uluru - Kata Tjuta National Park. Uluru is " +
    "sacred to the Pitjantjatjara and Yankunytjatjara, the " +
    "Aboriginal people of the area. It has many springs, waterholes, " +
    "rock caves and ancient paintings. Uluru is listed as a World " +
    "Heritage Site.</p>" +
    '<p>Attribution: Uluru, <a href="https://en.wikipedia.org/w/index.php?title=Uluru&oldid=297882194">' +
    "https://en.wikipedia.org/w/index.php?title=Uluru</a> " +
    "(last visited June 22, 2009).</p>" +
    "</div>" +
    "</div>";

      const infowindow = new google.maps.InfoWindow({
        content: contentString,
      });

      marker.addListener("click", (event) => {
        var index = locationIndex(event.latLng.toString());
        clearMarkers();
        markers.splice(index, 1);
        for (let i = index; i < markers.length; i++) {
            markers[i].title = (markers[i].title - 1).toString();
            markers[i].label = (markers[i].label - 1).toString();
        }
        showMarkers();
        deleteUl();
        var ul = document.getElementById("list");
        for (let i = 0; i < markers.length; i++){
            var li = document.createElement("li");
            li.appendChild(document.createTextNode(markers[i].title + " - " + markers[i].position.toString()));
            ul.appendChild(li);
        }
      });

//      marker.addListener("mouseover", function() {
//        infowindow.open(map, marker);
//      });

    var ul = document.getElementById("list");
    var li = document.createElement("li");
    li.appendChild(document.createTextNode(marker.title + " - " + marker.position.toString()));
    ul.appendChild(li);

    markers.push(marker);
  }

}

function compareLocation(position) {
    for (let i = 0; i < markers.length; i++) {
        if (markers[i].position.toString() == position) {
            return true;
        }
    }
    return false;
}

function locationIndex(position) {
    for (let i = 0; i < markers.length; i++) {
        if (markers[i].position.toString() == position) {
            return i;
        }
    }
    return -1;
}

// Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (let i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}

// Shows any markers currently in the array.
function showMarkers() {
  setMapOnAll(map);
}

// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  deleteUl();
  markers = [];
}

function deleteUl() {
  var ul = document.getElementById("list");
  ul.innerHtml = "";
  ul.innerText = "";
}




function sendRoute() {
    if (markers.length != 0) {
        var ul = document.getElementById("list").innerText;
        //console.log(document.getElementById("description_area").value);
        ul += "\nDescription:" + document.getElementById("description_area").value;
        var xhttp = new XMLHttpRequest();
        xhttp.open("POST", "route", true);
        xhttp.send(ul);
        alert("check the route if saved and send a socket to the client!!");
    } else {
        alert("can't save an empty route");
    }
}




