import pyodbc


def UPDATE(qry, tbl):
    try:
        innerFunc(qry, tbl)
        return True
    except Exception as e:
        print(e)
        return False


def DELETE(qry, tbl):
    try:
        innerFunc(qry, tbl)
        return True
    except Exception as e:
        print(e)
        return False


def INSERT(qry, tbl):
    try:
        innerFunc(qry, tbl)
        return True
    except Exception as e:
        print(e)
        return False


def SELECT(qry):
    conn = connect()

    cursor = conn.cursor()
    try:
        cursor.execute(qry)
        return cursor
    except:
        return None


def connect():
    return pyodbc.connect(
        'Driver={ODBC Driver 17 for SQL Server};Server=DESKTOP-U1GRQB7;Database=travel_db;Trusted_Connection=yes;')


def innerFunc(qry, tbl):
    conn = connect()

    cursor = conn.cursor()
    cursor.execute('SELECT * FROM ' + tbl)

    cursor.execute(qry)
    conn.commit()
