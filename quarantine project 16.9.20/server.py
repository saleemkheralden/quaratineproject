from __future__ import absolute_import, division, print_function, unicode_literals
from flask import Flask, request, render_template, send_file, redirect, session, g, make_response, url_for
import os
from SQL_Wrapper.sql import INSERT, UPDATE, DELETE, SELECT
from werkzeug.security import generate_password_hash, check_password_hash
from flask_socketio import SocketIO, send, emit, join_room, leave_room
from datetime import datetime
import random
import subprocess
import socket

app = Flask(__name__)

app.secret_key = os.urandom(24)

localhost = "127.0.0.1"
publicIp = "82.81.32.217"
try:
    ip = socket.gethostbyname(socket.gethostname())
except:
    ip = localhost
port = 443
domainName = ".com"
# connectionStr = "http://{}".format(domainName)
connectionStr = "https://{}:{}".format(ip, port)
login_flag = True


# main page before login
@app.route('/')
def index():
    return render_template('index.html', dns=connectionStr)


# home page (after login)
@app.route('/home', methods=['POST', 'GET'])
def home():
    if 'user' in session:
        qry = 'SELECT TOP 10 * FROM routes ORDER BY id DESC'.format(session['user']['id'])
        ds = SELECT(qry)
        posts = get_json(ds)
        return render_template('mainpage.html', dns=connectionStr, posts=posts)
    global login_flag
    login_flag = True
    return redirect('/login')


# creates a json structure for the client js to read
# json template :
# [
# 	{
# 		"name": "",
# 		"date": "",
# 		"description": "",
# 		"route": ""
# 	},
# 	{
# 		"name": "",
# 		"date": "",
# 		"description": "",
# 		"route": ""
# 	}....
# ]
def get_json(ds):
    json = list()
    for e in ds:
        name = ''
        user_id = e[1]
        route_file = e[2]
        date = e[4]
        description = e[5]
        description = description.replace('\n', '<br>')
        qry = "SELECT username FROM users WHERE id={}".format(user_id)
        user_ds = SELECT(qry)
        for user_e in user_ds:
            name = user_e[0]
        file = open(route_file, 'r')
        file_route = file.read()
        file_route = file_route.split('\n')
        route = list()
        for line in file_route:
            route.append(extract_coord(line))
        data = {"name": name,
                "date": date.strftime('%d-%m-%Y'),
                "description": description,
                "route": route}
        json.append(data)
    return json


# get a line (lat, lng) of coordinates and returns a tuple with the latitude and the longitude
def extract_coord(line):
    line = line.split('(')[1]
    line = line.split(')')[0]
    lat = line.split(',')[0]
    lng = line.split(' ')[1]
    return (lat, lng)


# profile page
# view,edit profile
profile_updated = False


@app.route('/profile', methods=['POST', 'GET'])
def profile():
    global profile_updated
    if request.method == 'POST':
        name = request.form['name-input']
        last_name = request.form['last_name-input']
        username = request.form['username-input']
        email = request.form['email-input']
        phone_number = request.form['phone_number-input']
        qry = "UPDATE users SET name='{}', last_name='{}', username='{}', email='{}', phone_number='{}' WHERE id={}".format(name,
                                                                                                                                last_name,
                                                                                                                                username,
                                                                                                                                email,
                                                                                                                                phone_number,
                                                                                                                                session["user"]["id"])

        if UPDATE(qry, 'users'):
            session["user"] = get_user_data(username)
            session.update = True
            profile_updated = True
            return redirect('/profile')

    if 'user' in session:
        if profile_updated:
            profile_updated = False
            return render_template('profile.html', user=session["user"], updated='Profile was updated successfully!')
        return render_template('profile.html', user=session["user"])
    global login_flag
    login_flag = True
    return redirect('/login')


# route page
# addes routes and create posts
@app.route('/route', methods=['POST', 'GET'])
def add_route():
    if request.method == 'POST':
        data = request.data.decode().split("\nDescription:")
        description = data[1]
        route = data[0]
        id_code = random.randint(10000000, 99999999)
        date = datetime.now().strftime('%d%m%Y%H%M%S')
        path = 'route_data\\' + session['user']['id'].__str__() + '_' + date + '_' + id_code.__str__() + '.txt'
        with open(path, 'w') as f:
            f.write(route)
            f.close()

            # insert into database
            qry = "INSERT INTO routes (user_id, route_file, id_code, route_date, description) VALUES ({}, '{}', '{}', '{}', '{}')".format(
                session['user']['id'], os.path.abspath('') + '\\' + path, id_code, datetime.now().strftime('%m-%d-%Y'),
                description)
            INSERT(qry, 'routes')
            # / file = open(os.path.abspath('') + '\\' + path, 'r')
            # data = file.read()
            # file.close()
            # print(os.path.abspath('') + '\\' + path)
            # print(data)

    if 'user' in session:
        return render_template('add_route.html', dns=connectionStr)
    global login_flag
    login_flag = True
    return redirect('/login')


# login page
@app.route('/login', methods=['POST', 'GET'])
def login():
    global login_flag
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        try:
            remember_me = request.form['rememberMeBox']
        except:
            remember_me = 'off'

        if login_handle(username, password):
            login_flag = True
            if remember_me == 'on':
                resp = make_response(redirect('/home'))
                resp.set_cookie('user',
                                value=username,
                                max_age=60 * 60 * 24 * 10,
                                path='/',
                                domain=None,
                                secure=False,
                                httponly=False
                                )
                return resp
            return redirect('/home')
        else:
            login_flag = False
            return redirect('/login', code=302)

    cookie = request.cookies
    user = cookie.get('user')
    if user is not None:
        session['user'] = get_user_data(user)
        return redirect('/home')

    if not login_flag:
        login_flag = True
        return render_template('login.html', dns=connectionStr, login_err_msg='username or password is not correct')
    return render_template('login.html', dns=connectionStr)


# logs in the user and sets up the session
def login_handle(username, password):
    user_data = get_user_data(username)
    if user_data is not None:
        if check_password_hash(user_data['password'], password):
            session['user'] = user_data
            return True
    return False


# returns the users data to store in the session
def get_user_data(username):
    table = "users"
    qry = "SELECT * FROM {} WHERE username='{}'".format(table, username)
    cursor = SELECT(qry)
    if cursor is not None:
        for data in cursor:
            user_data = {"id": data[0],
                         "username": data[1],
                         "password": data[2],
                         "name": data[3],
                         "last_name": data[4],
                         "email": data[5],
                         "phone_number": data[6],
                         "admin": data[7]}
            return user_data
    return None


# logs the user out
# and removes the session content
@app.route('/logout')
def logout():
    if 'user' in session:
        session.pop('user', None)
    resp = make_response(redirect('/'))
    resp.set_cookie('user', '', max_age=0)
    return resp


# register page
# list of variables:
# username = request.form['username_field']
# password = request.form['password_field']
# conf_password = request.form['conf_password_field']
# name = request.form['name_field']
# last_name = request.form['last_name_field']
# email = request.form['email_field']
# phone_number = request.form['phone_number_field']
register_flag = True
register_err_msg = ''


@app.route('/register', methods=['POST', 'GET'])
def register():
    global register_flag, register_err_msg
    if request.method == 'POST':
        username = request.form['username_field']
        password = request.form['password_field']
        conf_password = request.form['conf_password_field']
        name = request.form['name_field']
        last_name = request.form['last_name_field']
        email = request.form['email_field']
        phone_number = request.form['phone_number_field']
        # print(username)
        # print(password)
        # print(conf_password)
        # print(name)
        # print(last_name)
        # print(email)
        # print(phone_number)

        if password == conf_password:
            if check_user_exist(username):
                register_flag = False
                register_err_msg = 'username already exists'
                return redirect('/register')
            else:
                register_flag = True
                if add_user(username, password, name, last_name, email, phone_number):
                    global login_flag
                    login_flag = True
                    return redirect('/login')
                else:
                    register_flag = False
                    register_err_msg = "Try again later!"
                    return redirect('/register')
        else:
            register_flag = False
            register_err_msg = "passwords doesn't match"
            return redirect('/register')

    if not register_flag:
        register_flag = True
        return render_template('/register.html', register_err_msg=register_err_msg)
    return render_template('/register.html')


# might not use
# checks the data given by the user
# and checks if it's not linked to another user
#
def check_data(username, password, conf_password, phone_number):
    if conf_password == password:
        if not check_user_exist(username):
            ds = SELECT("SELECT phone_number FROM users WHERE phone_number='{}'".format(phone_number))
            if ds is None:
                return True
            else:
                return "Phone number already linked to another account!"
        else:
            return "Username already exists"
    return False


# adds user into the database used to register user
def add_user(username, password, name, last_name, email, phone_number):
    table = "users"
    qry = "INSERT INTO {} ([username], [password], [name], [last_name], [email], [phone_number], [admin]) VALUES ('{}', '{}', '{}', '{}', '{}', '{}', 0)".format(table, username,
                                                                                           generate_password_hash(
                                                                                               password,
                                                                                               method='sha256'), name, last_name, email, phone_number)
    return INSERT(qry, table)


# checks if this username exists in the database
def check_user_exist(username):
    table = "users"
    qry = "SELECT * FROM {} WHERE username='{}'".format(table, username)
    cursor = SELECT(qry)
    for elem in cursor:
        return True
    return False


# navigation bar and layouts that needs to be added to multiple pages
# used before the user is logged in
@app.route('/include')
def include():
    return render_template('include.html', dns=connectionStr)


# navigation bar and layouts that needs to be added to multiple pages
# used after the user is logged in
@app.route('/include_after_login', methods=['POST', 'GET'])
def include_after_login():
    return render_template('include_after_login.html', dns=connectionStr, admin_tab="<li class='left'><a href='/sha256$N9qYHMSF$dd2285b5110dcc3ee6f3e575edade0a32c3ea54831e0884274089637d47b5801' id='admin-link' class='underline'><span>Admin</span></a></li>") if session['user']['admin'] else render_template('include_after_login.html', dns=connectionStr)


# the about us page
@app.route('/about')
def about():
    return render_template('about.html', dns=connectionStr)


# returns the tab icon
@app.route('/favicon.ico')
def icon():
    return send_file('static/images/favicon.ico')


# admin page
@app.route('/sha256$N9qYHMSF$dd2285b5110dcc3ee6f3e575edade0a32c3ea54831e0884274089637d47b5801')
def admin_page():
    if 'user' in session:
        if session['user']['admin']:
            return render_template('admin.html', users=get_all_users())
    return redirect('/home')


# creates a json structure for the client js to read
# json template :
# [
# 	{
# 		"id": (int),
# 		"username": "",
# 		"password": "",
# 		"name": "",
# 		"last_name": "",
# 		"email": "",
# 		"phone_number": "",
# 		"admin": (bool)
# 	},
# 	{
# 		"id": (int),
# 		"username": "",
# 		"password": "",
# 		"name": "",
# 		"last_name": "",
# 		"email": "",
# 		"phone_number": "",
# 		"admin": (bool)
# 	}....
# ]
def get_all_users():
    json = list()
    qry = "SELECT * FROM users"
    ds = SELECT(qry)
    for e in ds:
        data = {"id": e[0],
              "username": e[1],
              "password": e[2],
              "name": e[3],
              "last_name": e[4],
              "email": e[5],
              "phone_number": e[6],
              "admin": e[7]}
        json.append(data)
    return json


# runs the server, can add additional code to run before the server is up.
def run():
    app.run(ip, port=port, debug=True, ssl_context=('server.crt', 'server.key'))


if __name__ == "__main__":
    run()

    # <div class="outer_div">
    #     <div class="inner_div">
    #         <div>
    #             <span id="name_span">
    #                 Name
    #             </span>
    #         </div>

    #         <div>
    #             <span id="date_span">
    #                 Date
    #             </span>
    #         </div>

    #         <div id="description_div">
    #             <p id="description_par">
    #                 Description
    #             </p>
    #         </div>
    #     </div>

    #     <div class="inner_div">
    #         <div>
    #             MAP
    #         </div>
    #     </div>
    # </div>
